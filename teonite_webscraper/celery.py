import os

from celery import Celery

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'teonite_webscraper.settings')

app = Celery('teonite_webscraper')
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()
