import nltk
from django.test import TestCase
from django.urls import reverse_lazy
from scraper.helpers import sort_dict, word_counter
from scraper.models import Article, Author

nltk.download('punkt')
nltk.download('stopwords')


class HelpersTestCase(TestCase):

    def setUp(self):
        pass

    def test_counter_counts_correctly(self):
        sentence = 'A test sentence with two test words.'
        counted = {}
        counted = word_counter(sentence, counted)
        self.assertEqual(counted, {'test': 2, 'sentence': 1, 'two': 1, 'words': 1})

    def test_sort_sorts_correctly(self):
        test_case = {'a': 2, 'b': 5, 'c': 1, 'd': 3, 'e': 4}
        sorted_case = sort_dict(test_case)
        self.assertEqual(sorted_case, {'b': 5, 'e': 4, 'd': 3, 'a': 2, 'c': 1})


class ViewsTestCase(TestCase):

    def setUp(self):
        new_author = Author.objects.create(fullname='test author', stub='test', most_used_words={})
        article = Article.objects.create(author=new_author, content='This is a test sentence with two test words.')
        new_author.most_used_words = word_counter(article.content, new_author.most_used_words)
        new_author.save()

    def test_counts_words_properly(self):
        response = self.client.get(reverse_lazy('stats'))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(),
                         {'test': 2, 'two': 1, 'words': 1, 'sentence': 1})

    def test_shows_authors(self):
        response = self.client.get(reverse_lazy('authors'))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), {'test': 'test author'})

    def test_count_words_by_author(self):
        response = self.client.get(reverse_lazy('author_most_used_words', kwargs={'stub': 'test'}))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(),
                         {'test': 2, 'two': 1, 'words': 1, 'sentence': 1})
