from django.contrib.postgres.fields import JSONField
from django.db import models


class Author(models.Model):
    fullname = models.CharField(max_length=100)
    stub = models.CharField(max_length=100)
    most_used_words = JSONField(blank=True)

    def __str__(self):
        return self.fullname


class Article(models.Model):
    author = models.ForeignKey(Author, on_delete=models.CASCADE)
    link = models.CharField(max_length=200)
    content = models.TextField()
    title = models.CharField(max_length=200)

    def __str__(self):
        return self.title
