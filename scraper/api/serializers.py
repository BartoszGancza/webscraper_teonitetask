from rest_framework import serializers
from scraper.helpers import sort_dict


class AuthorWordsSerializer(serializers.ModelSerializer):
    def to_representation(self, obj):
        return sort_dict(obj.most_used_words)
