from rest_framework import generics, permissions, status
from rest_framework.response import Response
from scraper.api.serializers import AuthorWordsSerializer
from scraper.helpers import sort_dict
from scraper.models import Author


class AuthorAPIView(generics.ListAPIView):
    queryset = Author.objects.all()
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

    def list(self, request, *args, **kwargs):
        queryset = self.get_queryset()
        response = {}
        for author in queryset:
            response[author.stub] = author.fullname
        if response:
            return Response(response)
        else:
            return Response(data={'detail': 'Not found.'}, status=status.HTTP_404_NOT_FOUND)


class AuthorWordsAPIView(generics.RetrieveAPIView):
    queryset = Author.objects.all()
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
    serializer_class = AuthorWordsSerializer
    lookup_field = 'stub'


class AllWordsAPIView(generics.ListAPIView):
    queryset = Author.objects.all()
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

    def list(self, request, *args, **kwargs):
        queryset = self.get_queryset()
        response = {}
        for author in queryset:
            for word, count in author.most_used_words.items():
                if word in response:
                    response[word] += count
                else:
                    response[word] = count
        if response:
            return Response(sort_dict(response))
        else:
            return Response({'detail': 'Not found.'}, status=status.HTTP_404_NOT_FOUND)
