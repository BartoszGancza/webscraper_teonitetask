from django.core.management.base import AppCommand


class Command(AppCommand):
    help = 'Scraps the web'

    def handle_app_config(self, app_config, **options):
        import requests
        import nltk
        from bs4 import BeautifulSoup
        from scraper.helpers import get_links, word_counter
        from scraper.models import Article, Author
        from text_unidecode import unidecode

        nltk.download('punkt')
        nltk.download('stopwords')

        self.stdout.write('Starting...')
        # For implementation check helpers.py, grabs all the article links from blog
        links = get_links('https://teonite.com/blog/')
        # List of objects to batch inject into DB to save I/Os
        objects_to_inject = []

        links_in_db = list(Article.objects.all().values_list('link', flat=True))
        authors_in_db = list(Author.objects.all().values_list('stub', flat=True))

        for link in links:

            if link not in links_in_db:
                # Grab article page
                blog_post = requests.get(link)
                # Prepare soup
                soup = BeautifulSoup(blog_post.content, 'lxml')
                # Gets the json with author data from page meta
                author_name = soup.find('span', class_='author-name').get_text()
                author_stub = unidecode(author_name).replace(' ', '').lower()

                # All of the below can be done within Articles() as parameters, but for clarity
                # I prefer separate lines.
                post_data = Article()
                post_data.link = link
                post_data.content = soup.find('div', class_='post-content').get_text()

                # Check if author is already in DB if so assign the key.
                if author_stub in authors_in_db:
                    author = Author.objects.get(stub=author_stub)
                    post_data.author = author
                else:
                    # If not, create new DB Authors item and then assign.
                    author = Author(fullname=author_name, stub=author_stub, most_used_words={})
                    author.save()
                    # Unlike links which are unique, author might appear many times and we only grab
                    # them from DB once at the beginning, so adding it here to the checklist to avoid trying to
                    # add same author multiple times
                    authors_in_db.append(author_stub)
                    post_data.author = author

                post_data.title = soup.find('h1', class_='post-title').get_text()

                author.most_used_words = word_counter(post_data.content,
                                                      author.most_used_words)
                author.save()

                # Append object to the list and continue
                objects_to_inject.append(post_data)

        Article.objects.bulk_create(objects_to_inject)
        self.stdout.write('Done collecting data!')
