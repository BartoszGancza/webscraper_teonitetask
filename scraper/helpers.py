import os
from collections import Counter

import nltk
import requests
from bs4 import BeautifulSoup
from nltk.corpus import stopwords


def get_links(starting_page):
    first_page = requests.get(starting_page)
    soup = BeautifulSoup(first_page.content, 'lxml')

    # Gets the total number of pages to scrape articles from to avoid hard coding
    number_of_pages = soup.find_all('span', class_='page-number')[0].get_text()[2]

    # Grabs the first batch of links from the 1st page of the blog
    links = []
    # Use links embedded into article title for easiness
    article_titles = soup.find_all('h2', class_='post-title')

    for link in article_titles:
        for child in link:
            links.append('https://teonite.com' + child.get('href')[2:])

    # Go to other pages to grab the rest of the links
    for i in range(2, int(number_of_pages) + 1):

        next_page = requests.get('https://teonite.com/blog/page/' + str(i))

        soup = BeautifulSoup(next_page.content, 'lxml')
        article_titles = soup.find_all('h2', class_='post-title')

        for link in article_titles:
            for child in link:
                links.append('https://teonite.com' + child.get('href')[8:])

    return links


def sort_dict(dictionary):
    # Sorts the dict by values, returning tuples and puts it back into a dict
    sorted_by_value = dict(sorted(dictionary.items(), key=lambda kv: kv[1], reverse=True)[:10])
    return sorted_by_value


def word_counter(post_content, word_count):
    words = nltk.word_tokenize(post_content, language='english')
    # Clean stopwords and punctuation
    stop_words = stopwords.words('english')
    cleaned_list = [word.lower() for word in words if word.lower() not in stop_words and word.isalpha()]
    count = Counter(cleaned_list)

    # Add words and values to dictionary or update the count for already added words
    for key, value in count.items():
        if key in word_count:
            word_count[key] += value
        else:
            word_count[key] = value

    return word_count


def run_scraping():
    command = 'python manage.py scrape scraper'
    os.system(command)
