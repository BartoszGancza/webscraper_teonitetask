from django.urls import path
from scraper.api.views import (AllWordsAPIView, AuthorAPIView,
                               AuthorWordsAPIView)

urlpatterns = [
    path('stats/<stub>', AuthorWordsAPIView.as_view(), name='author_most_used_words'),
    path('stats/', AllWordsAPIView.as_view(), name='stats'),
    path('authors/', AuthorAPIView.as_view(), name='authors'),
]
