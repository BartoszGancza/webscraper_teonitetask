from celery import shared_task
from scraper.helpers import run_scraping


@shared_task
def run_scraper():
    run_scraping()
