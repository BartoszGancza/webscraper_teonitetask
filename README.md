# Bartek's Teonite Blog Scraper
This project is created and maintained by Bartosz Gancza.

The main purpose of the project is to scrape a Teonite (company) blog aand count most commonly
used words in articles per author and in general.

## Run the project
### Using docker

To run the project using Docker, ensure you have `Docker` and `docker-compose` installed (on Linux systems `docker-compose`
needs to be installed separately), and that the Docker service is running.

It is advised you use the latest stable version of `Docker`, but to run this project you have to at least have a 
version that supports `docker-compose.yml` version `3.7`.

To start the container:
```bash
docker-compose up
```

To stop all services:
```bash
docker-compose stop
```
in a terminal session not currently running services (or `CTRL+C` in current terminal window).

To remove all data, volumes and networks associated with containers (testing after DB changes, etc.):
```bash
docker-compose down -v
docker system prune -a
```
#### Running management commands inside Docker container
Run:
```bash	
docker exec -it scraper bash
```
Now you are logged in to your container and can execute all management commands as normal.
### Running project natively
Please use Python 3.7

Create virtual environment:
```bash
pip install virtualenv
virtualenv -p python3 venv
```

Remember to activate virtualenv before starting the project, you can do it with command:
```bash
source venv/bin/activate
```

Install requirements:
```bash
pip install -r requirements.txt
```

Set following environment variables:
```bash
export SECRET_KEY=123
export DATABASE_URL=sqlite:///db.sqlite3
export DEBUG=True
```

Run migrations:
```bash
python3 manage.py migrate
```

Run development server:
```bash
python3 manage.py runserver
```

## Developing the app using best code practises with `pre-commit`
This application is using `pre-commit` to check code style before you make a commit, to ensure that tests fail early,
thus avoiding the application to fail on easy (stylistic) tests later on in the pipeline.

### Installation
Ensure you have `pre-commit` installed via executing:
```bash
pip install -r requirements.txt
```

Run:
```bash
pre-commit install
```
From now on, all tests set in the `.pre-commit-config.yaml` will run on every commit checking your code and stopping 
the commit if it finds errors.

### Modifying hooks
To modify the tests that will run on every commit edit `.pre-commit-config.yaml`.

Instructions and the list of available hooks can be found [here](https://pre-commit.com).
